//	Customization

var appPort = 8100;

// Librairies
var livereload = require('livereload');
var underscore = require('underscore');
var express = require('express'),
    cors = require('cors'),
    http = require('http'),
    server;

server = http.createServer(function(req,res){
	// Set CORS headers
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Request-Method', '*');
	res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
	res.setHeader('Access-Control-Allow-Headers', '*');
	if ( req.method === 'OPTIONS' ) {
		res.writeHead(200);
		res.end();
		return;
	};  
}); 
 
io = require('socket.io')(server, { origins: '*:*'});

// Render and send the main page
server.listen(appPort, "192.168.0.171");
console.log("Server listening on port " + appPort);

var users = 0; //count the users

io.on('connection', function (socket){
    var nb = 0;

    console.log('SocketIO > Connected socket ' + socket.id);
    socket.on('someaction', function() {
        console.log('ElephantIO broadcast > ');
    });

    socket.on('disconnect', function () {
        console.log('SocketIO : Received ' + nb + ' messages');
        console.log('SocketIO > Disconnected socket ' + socket.id);
    });
});
